#include "GeodesicShooting.h"
#include <time.h>

struct params
{
  int numStep;
  int lpower;
  int truncX;
  int truncY;
  int truncZ;
  float alpha;
  float gamma;
  int memType;
  MemoryType mType;
  bool doRK4;
};

int main(int argc, char** argv)
{
  char v0Path[500];
  char w0Path[500];

  int argi = 1;
  strcpy(v0Path, argv[argi++]);
  strcpy(w0Path, argv[argi++]);

  params parms;
  parms.numStep = atoi(argv[argi++]);
  parms.lpower = atoi(argv[argi++]);
  parms.truncX = atoi(argv[argi++]);
  parms.truncY = atoi(argv[argi++]);
  parms.truncZ = atoi(argv[argi++]);
  parms.alpha = atof(argv[argi++]);
  parms.gamma = atof(argv[argi++]);
  parms.memType = atoi(argv[argi++]);
  parms.doRK4 = atoi(argv[argi++]) > 0;

  // precalculate low frequency location
  if (parms.truncX % 2 == 0) parms.truncX -= 1; // set last dimension as zero if it is even
  if (parms.truncY % 2 == 0) parms.truncY -= 1;
  if (parms.truncZ % 2 == 0) parms.truncZ -= 1;

  // runs on CPU or GPU
  if (parms.memType == 0)
    parms.mType = MEM_HOST;
  else
    parms.mType = MEM_DEVICE;

  // read data
  Field3D *v0Spatial = new Field3D(parms.mType);
  Field3D *w0Spatial = new Field3D(parms.mType);
  ITKFileIO::LoadField(*v0Spatial, v0Path);
  ITKFileIO::LoadField(*w0Spatial, w0Path);
  GridInfo grid = v0Spatial->grid();
  Vec3Di mSize = grid.size();

  int fsx = mSize.x;
  int fsy = mSize.y;
  int fsz = mSize.z;

  FftOper *fftOper = new FftOper(parms.alpha, parms.gamma, parms.lpower, grid,
                                 parms.truncX, parms.truncY, parms.truncZ);
  fftOper->FourierCoefficient();
  FieldComplex3D *v0 = new FieldComplex3D(parms.truncX, parms.truncY, parms.truncZ);
  FieldComplex3D *w0 = new FieldComplex3D(parms.truncX, parms.truncY, parms.truncZ);
  FieldComplex3D *parTransvw = new FieldComplex3D(parms.truncX, parms.truncY, parms.truncZ);
  Field3D *parTransvwSpatial = new Field3D(grid, parms.mType);

  clock_t t;
  t = clock();

  fftOper->spatial2fourier(*v0, *v0Spatial);
  fftOper->spatial2fourier(*w0, *w0Spatial);

  GeodesicShooting *geodesicshooting = new GeodesicShooting(fftOper, parms.mType, parms.numStep, parms.doRK4);

  geodesicshooting->parallelTranslate(*parTransvw, *v0, *w0);

  fftOper->fourier2spatial(*parTransvwSpatial, *parTransvw);

  t = clock() - t;
  printf("It took me %lu clicks (%f seconds).\n",t,((float)t)/CLOCKS_PER_SEC);

  ITKFileIO::SaveField(*parTransvwSpatial, "wTranslatedInVDirection.mhd");


  delete fftOper;
  delete geodesicshooting;
  delete v0;
  delete w0;
  delete parTransvw;
  delete v0Spatial;
  delete w0Spatial;
  delete parTransvwSpatial;

}
